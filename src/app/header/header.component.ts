import { Subscription } from 'rxjs/internal/Subscription';
import { AuthService } from './../auth/auth.service';
import { Component, EventEmitter, Output, OnInit, OnDestroy } from '@angular/core';
import { DataStorageService } from '../shared/data-storage-service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit, OnDestroy {
  constructor(private dataStorage: DataStorageService, private authService: AuthService){}
  private userSub = new Subscription;
  isAuthenticated = false;
  onSave(){
    this.dataStorage.storeRecipes();
  }
  onFetch(){
    this.dataStorage.fetchRecipes().subscribe();
  }

  ngOnInit(){
   this.userSub= this.authService.user.subscribe(user => {
    //  this.i
   });
  }
  ngOnDestroy(){
    this.userSub.unsubscribe();
  }
}
