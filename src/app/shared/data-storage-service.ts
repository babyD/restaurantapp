import { RecipeService } from './../services/recipe.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Recipe } from '../recipes/recipe.model';
import {map, tap} from 'rxjs/operators';
@Injectable({providedIn:'root'})
export class DataStorageService{
    constructor(private http:HttpClient, private recipeService: RecipeService){}

    storeRecipes(){
        const recipes = this.recipeService.getRecipe();
        return this.http.put('https://recipe-book-4c0f3.firebaseio.com/recipe.json', recipes)
        .subscribe(response =>{
            console.log(response);
        })
    }

    fetchRecipes(){
       return this.http.get<Recipe[]>('https://recipe-book-4c0f3.firebaseio.com/recipe.json')
        .pipe(
            map(recipes =>{
                return recipes.map(recipe =>{
                    return {

                        ...recipe,
                        ingredients: recipe.ingredients ? recipe.ingredients : []
                    }
                })
            }),
            tap(recipes => {
                this.recipeService.setRecipe(recipes);
              })
        )
        // .subscribe(recipes =>{
        //     // console.log(response);
        //     this.recipeService.setRecipe(recipes)
        // })
    }
}