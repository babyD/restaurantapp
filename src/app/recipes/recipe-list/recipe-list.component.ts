import { Subscription } from 'rxjs/internal/Subscription';
import { RecipeService } from './../../services/recipe.service';
import { Component, OnInit, EventEmitter, Output, OnDestroy } from '@angular/core';

import { Recipe } from '../recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit, OnDestroy {

  recipes: Recipe[];
  recipeSubsription:Subscription;
  constructor(private recipeService:RecipeService) { }


  ngOnInit() {
   this.recipeSubsription= this.recipeService.recipesChanged
    .subscribe(
      (recipes: Recipe[])=>{
        this.recipes=recipes;
      }
    )
    this.recipes=this.recipeService.getRecipe();
  }
  ngOnDestroy(){
    this.recipeSubsription.unsubscribe();

  }



}
