import { Subject } from 'rxjs';
import { ShopListService } from './shop-list.service';
import { Injectable, EventEmitter } from '@angular/core';
import { Recipe } from '../recipes/recipe.model';
import { Ingredient } from '../shared/ingredient.model';


@Injectable()
export class RecipeService {
  // recipeSelected = new Subject<Recipe>();
  recipesChanged = new Subject<Recipe[]>();
//  private  recipes: Recipe[] = [
//     new Recipe('Waakye', 'This is simply a test', '../../../assets/image/waakye.jpg',
//     [
//       new Ingredient('Rice',3),
//       new Ingredient('beans',1)
//     ]),
//     new Recipe('Tuozaafi', 'This is simply a test', '../../../assets/image/tz.jpg',
//     [
//       new Ingredient('flour',2),
//       new Ingredient('soup',1)
//     ]),
//     new Recipe('Redred', 'This is simply a test', '../../../assets/image/RedRed.jpg',
//     [
//       new Ingredient('plaintain',5),
//       new Ingredient('beans',2),
//       new Ingredient('oil',1),
//     ]),
//     new Recipe('Akyeke', 'This is simply a test', '../../../assets/image/akyeke.jpg',
//     [new Ingredient('maize',3),
//     new Ingredient('fish',1)
//   ])
//   ];
  private recipes: Recipe[] =[];
  constructor(private ListService: ShopListService) { }
  setRecipe(recipes : Recipe[]){
    this.recipes = recipes;
    this.recipesChanged.next(this.recipes.slice())
  }
  getRecipe(){
    //return a new array which is an exact copy of the array above
    return this.recipes.slice();
  }


  getRecipes(index:number){
    return this.recipes.slice()[index]
  }
  
 

  onAddList(ingredients:Ingredient[]){
    this.ListService.addIngredients(ingredients)
  }
  addRecipe(recipe: Recipe){
    this.recipes.push(recipe)
    this.recipesChanged.next(this.recipes.slice())
  }
  updateRecipe(index:number, newRecipe:Recipe){
    this.recipes[index]=newRecipe;
    this.recipesChanged.next(this.recipes.slice())
  }
  deleteRecipe(index:number){
    this.recipes.splice(index,1);
    //emit new recipe
    this.recipesChanged.next(this.recipes.slice())
  }
}
