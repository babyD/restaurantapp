import { Subscription } from 'rxjs/internal/Subscription';
import { ShopListService } from './../../services/shop-list.service';
import {
  Component,
  OnInit,
  ElementRef,
  ViewChild,
  EventEmitter,
  Output,
  OnDestroy
} from '@angular/core';

import { Ingredient } from '../../shared/ingredient.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';


@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
  formgroup: FormGroup
  // @ViewChild('formGroup',{static:false}) slForm:FormGroup
  subscription: Subscription;
  editMode = false;
  editedItemIndex: number;
  editedItem: Ingredient;
  constructor(private slService: ShopListService) { }

  ngOnInit() {
    this.formgroup = new FormGroup(
      {
        name: new FormControl(null, Validators.required),
        amount: new FormControl(null, Validators.required)
      }
    )
    this.subscription =  this.slService.startedEditing.subscribe(
      (index:number)=>{
        this.editedItemIndex=index;
        this.editMode = true;
        this.editedItem = this.slService.getIngredient(index)
        this.formgroup.setValue(
          {
            name:this.editedItem.name,
            amount:this.editedItem.amount
          }
        )
      }
    );
  }
  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.subscription.unsubscribe();
  }

  
  onAddItem(form: FormGroup) {
    const value = this.formgroup.value;
    // console.log(value)
    // const ingName = this.nameInputRef.nativeElement.value;
    // const ingAmount = this.amountInputRef.nativeElement.value;
    const newIngredient = new Ingredient(value.name , value.amount);
    if(this.editMode){
      this.slService.updateIngredient(this.editedItemIndex, newIngredient)
      
      this.editMode=false;
    }
    else{
      this.slService.addIngredient(newIngredient);
    }
    this.formgroup.reset();
    console.log(newIngredient);
  }
  onClear(){
    this.formgroup.reset();
    this.editMode=false;
  }
  onDelete(){
    this.slService.deleteIngredient(this.editedItemIndex)
  }

}
