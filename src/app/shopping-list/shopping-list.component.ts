import { Subscription } from 'rxjs/internal/Subscription';
import { ShopListService } from './../services/shop-list.service';
import { Component, OnInit } from '@angular/core';

import { Ingredient } from '../shared/ingredient.model';


@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {
  ingredients: Ingredient[];
  
  constructor(private shopList:ShopListService) { }

  ngOnInit() {
    this.ingredients=this.shopList.getIngredients();
    this.shopList.ingredientsChanged
    .subscribe(
      (ingredients: Ingredient[])=>{
        this.ingredients =ingredients;
      }
    )
   
    
  }
  
  onIngredientAdded(ingredient: Ingredient) {
    this.ingredients.push(ingredient);
  }
  onEditItem(index: number){
    this.shopList.startedEditing.next(index);

  }
}
