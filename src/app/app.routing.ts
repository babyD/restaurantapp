import { AuthComponent } from './auth/auth.component';
import { RecipesResolverService } from './recipes/recipes.resolvers.service';
import { RecipeEditComponent } from './recipes/recipe-edit/recipe-edit.component';
import { RecipeDetailComponent } from './recipes/recipe-detail/recipe-detail.component';
import { RecipeStartComponent } from './recipes/recipe-start/recipe-start.component';
import { ShoppingListComponent } from './shopping-list/shopping-list.component';
import { RecipesComponent } from './recipes/recipes.component';
import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';



const routes: Routes = [
    {  
        path: '', 
        redirectTo:'recipes',
        pathMatch:'full'
    }, 
    {
        path:'auth',
        component: AuthComponent
    },
    { 
        path: 'recipes', 
        component: RecipesComponent,
        children:[
            {
                path:'',
                component: RecipeStartComponent
            },

            {
                path:'new',
                component:RecipeEditComponent
            },
            {
                path:':id/edit',
                component:RecipeEditComponent,
                resolve: [RecipesResolverService]
            },
            {
                path:':id', 
                component:RecipeDetailComponent,
                resolve: [RecipesResolverService]
            },
           
           
        ]
    },
    { 
        path: 'shopping-list', 
        component: ShoppingListComponent 
    },
    
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {}
