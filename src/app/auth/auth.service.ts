import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { catchError, tap } from 'rxjs/operators';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { throwError, Subject } from 'rxjs';
import { User } from './user.model';

export interface AuthResponseData{
    kind: string;
    idToken: string;
    email: string;
    refreshToken: string;
    expiresIn: string;
    localId: string;
    registered?: boolean;
}
@Injectable({providedIn:'root'})
export class AuthService{
    constructor(private http: HttpClient){}
    user = new Subject<User>();
    signup(email: string, password:string){
        // tslint:disable-next-line: max-line-length
        return this.http.post<AuthResponseData>('https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyC_gAhzsaX4eg8EYgu9Te3urgLStpk-3M0'
      ,{
        email:  email,
        password: password,
        returnSecureToken: true
    }).pipe(catchError(this.handleError),
    tap(
        resData =>{
            this.handleAuthenticator(resData.email, +resData.expiresIn, resData.localId,resData.idToken)
        }
    )
    )
    }

    login(email: string, password:string){
        // console.log('we are here');
        return this.http.post<AuthResponseData>('https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyC_gAhzsaX4eg8EYgu9Te3urgLStpk-3M0',
        {
            email: email,
            password: password
        }
        
        ).pipe(catchError(this.handleError))
        // console.log(this.AuthResponseData)
       
    }
    private  handleAuthenticator(email, localId, idToken, expiresIn){
        const  expirationDate = new Date(
            new Date().getTime() + +expiresIn *100
        );
        const user = new User(email, localId, idToken, expiresIn);
        this.user.next(user)
    }
    private handleError(errorRes: HttpErrorResponse){
       
            let errorMessage = 'An unknown error occurred';
            if(!errorRes.error.error){
                return throwError(errorMessage);
            }
            switch (errorRes.error.error.message) {
                case 'EMAIL_EXISTS':
                    errorMessage= 'Email already exists';
                    break;
                case 'EMAIL_NOT_FOUND':
                    errorMessage ='User is not registered or password is incorrect'
                    break;
               case 'INVALID_PASSWORD':
                   errorMessage = 'User is not registered or password is incorrect'
            }
            
            return throwError(errorMessage)
    } 
}