import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService, AuthResponseData } from './auth.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
// import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';


@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  form: FormGroup;

  isLoggin:boolean= true;
  isLOading: boolean=false;
  error:string;
  success:string;
  authObservable: Observable<AuthResponseData>;

  constructor(private service: AuthService, private route: Router) { }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required)
    })
  }

  onSwitch(){
    this.isLoggin = !this.isLoggin
   
  }
  onSubmit(){
    
    if(this.form.invalid){
      console.log('Form is invalid')
    }
    if(this.isLoggin){
      // login details
      this.isLOading = true;
      this.authObservable= this.service.login(this.form.value.email,this.form.value.password)
      // this.success ='Login successful'
      this.form.reset()
    }
    if(!this.isLoggin){
      this.isLOading = true;
      // sign up code
      if(this.form.valid){
        // console.log(this.form.value);
        this.authObservable=this.service.signup( this.form.value.email, this.form.value.password )
        // this.success ='User created successfully'
        this.form.reset()
      }
    }
    this.authObservable.subscribe(resData=>{
      console.log(resData)
      this.isLOading=false;
      this.error ='Success'
      this.route.navigate(['recipes'])
    },
    errorMessage=>{
      console.log(errorMessage);
      this.error = errorMessage;
      this.isLOading = false;
      
    }
    )
  
  }
}
